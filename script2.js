
// When the page loads, load the language file and set the text.
window.addEventListener('load', function() {
  // Get the user's language.
  var language = navigator.language || navigator.userLanguage;
  
  // Set the language selector to the user's language.
  document.getElementById('language-selector').value = language;

  // Load the language file.
  loadLanguageFile(language);
});

// When the selected language changes, reload the language file and update the text.
document.getElementById('language-selector').addEventListener('change', function(event) {
  var language = event.target.value;
  language = language.toLowerCase().substring(0, 2);
  console.log(language);
  loadLanguageFile(language);
});

function loadLanguageFile(language) {
  // Path to the language JSON files.
  var path = 'languages/';

  // Construct the URL to the appropriate JSON file.
  var url = path + language + '.json';

  console.log(url);
  // Fetch and load the language file.
  fetch(url)
    .then(response => response.json())
    .then(data => {
      // Save the data so it can be used to set the text on the page.
      window.language = data;

      // Now set the text.
      setText();
    })
    .catch(error => {
      // If an error occurs (like if the JSON file for the user's language doesn't exist), you could default to English or show an error message.
      console.error('Error:', error);
      loadLanguageFile('ro');
    });
}

function setText() {
// Get all elements that have a 'data-i18n' attribute.
var elements = document.querySelectorAll('[data-i18n]');

// Go through each element and set its text based on the language data.
elements.forEach(element => {
    var key = element.getAttribute('data-i18n');
    var text = window.language[key];

    // If the key exists in the language data, set the text.
    if (text) {
    element.innerText = text;
    }
});
}


function viewImage1(imagePath) {
  var img = new Image();
  img.src = imagePath;
  img.style.maxWidth = "500px"; // Set to your desired max width
  img.style.maxHeight = "500px"; // Set to your desired max height
  var viewer = document.getElementById('imageViewer');
  viewer.innerHTML = '';
  viewer.appendChild(img);
}


function viewImage2(imagePath) {
  var newWindow = window.open("", "Image Window", "width=500,height=500"); // opens a new window
  newWindow.document.write("<html><body><img src='" + imagePath + "' alt='Image' style='max-width:100%;max-height:100%;'></body></html>"); // writes HTML to display image in the new window
}
